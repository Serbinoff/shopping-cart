object Main extends App {

  calculation(List("Apple", "Orange", "Apple", "Apple"))

  def calculation(list: List[String]): Double = {
    val m = list.groupBy(identity).mapValues(_.size)
    (m.getOrElse("Apple", 0)/2*60 + m.getOrElse("Apple", 0)%2*60 + m.getOrElse("Orange", 0)/3*50 + m.getOrElse("Orange", 0)%3*25)/100.0
  }
}