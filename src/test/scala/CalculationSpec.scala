import Main
import org.scalatest.FlatSpec

class FirstSpec extends FlatSpec {
  "A List with fruits as input" should "return as output their final price considering offers" in {
    val res1: Double = Main.calculation(List("Apple", "Orange", "Apple", "Apple", "Orange", "Orange"))
    assert (res1 == 1.7)
    val res2: Double = Main.calculation(List("Apple", "Orange", "Apple", "Apple"))
    assert (res2 == 1.45)
    val res3: Double = Main.calculation(List("Apple", "Orange", "Apple", "Apple", "Orange", "Orange", "Apple", "Orange"))
    assert (res3 == 1.95)
    val res4: Double = Main.calculation(List())
    assert (res4 == 0)
    val res5: Double = Main.calculation(List("Apple", "Orange"))
    assert (res5 == 0.85)
  }
}